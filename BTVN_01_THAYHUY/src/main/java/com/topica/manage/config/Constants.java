package com.topica.manage.config;

/**
 * Application constants.
 */
public final class Constants {
    public static final String DIALECT = "hibernate.dialect";
    public static final String SHOW_SQL = "hibernate.show_sql";
    public static final String HBM2DDL = "hibernate.hbm2ddl.auto";
    public static final String SECOND_LEVEL_CACHE = "hibernate.cache.use_second_level_cache";
    public static final String QUERY_CACHE = "hibernate.cache.use_query_cache";
    private Constants() {}
}

package com.topica.manage.web.errors;

public class CourseAlreadyExistException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public CourseAlreadyExistException() {
        super(ErrorConstants.COURSE_ALREADY_EXIST_TYPE, "Course already exists!", "courseManagement", "course-exists");
    }
}
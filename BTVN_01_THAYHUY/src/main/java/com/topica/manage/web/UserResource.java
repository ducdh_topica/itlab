package com.topica.manage.web;

import com.topica.manage.bean.User;
import com.topica.manage.security.AuthoritiesConstants;
import com.topica.manage.service.UserService;
import com.topica.manage.service.dto.UserDTO;
import com.topica.manage.web.util.HeaderUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("/api/v1")
public class UserResource {
    private static final String ENTITY_NAME = "user";
    private static final String DESC = "DESC";
    private final UserService userService;

    public UserResource(UserService userService) {
        this.userService = userService;
    }

    /**
     * GET /user : get all user
     *
     * @return the ResponseEntity with status 200 (OK) and with body all user
     */
    @GetMapping("/user")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity listUser(@RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
                                   @RequestParam(name = "size", required = false, defaultValue = "10") Integer size,
                                   @RequestParam(name = "sort", required = false, defaultValue = "ASC") String sort) {
        Sort sortable = Sort.by("id").ascending();
        if (sort.equals(DESC)) {
            sortable = Sort.by("id").descending();
        }
        Pageable pageable = PageRequest.of(page, size, sortable);
        final Page<User> listUser = userService.findAll(pageable);
        return new ResponseEntity<>(listUser, HttpStatus.OK);
    }

    /**
     * POST  /user: Create a new user.
     *
     * @param userDTO the userDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userDTO, or with status 400 (Bad Request) if the user has already or not created.
     */
    @PostMapping("/user")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity createUser(@RequestBody UserDTO userDTO) {
        try {
            UserDTO result = userService.save(userDTO);
            return ResponseEntity.created(new URI("/api/v1/user/" + result.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId() + ""))
                    .body(result);
        } catch (Exception e) {
            return new ResponseEntity<>(Collections.singletonMap("createUserFailed", e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * PUT  /user : Updates an existing user.
     *
     * @param userDTO the userDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userDTO,
     * or with status 400 (Bad Request) if the userDTO is not valid and not update,
     */
    @PutMapping("/user")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity updateUser(@RequestBody UserDTO userDTO) {
        try {
            if (userDTO.getId() <= 0) {
                return new ResponseEntity<>(Collections.singletonMap("updateUserFailed", "User does not exist!"), HttpStatus.BAD_REQUEST);
            }
            Optional<UserDTO> result = userService.update(userDTO);
            return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.get().getId() + ""))
                    .body(Optional.of(result).get());
        } catch (Exception e) {
            return new ResponseEntity<>(Collections.singletonMap("updateUserFailed",
                    e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * GET  /user/:id : get the "id" user.
     *
     * @param id the id of the userDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<UserDTO> getUser(@PathVariable Long id) {
        Optional<UserDTO> userDTO = userService.findOne(id);
        return ResponseEntity.ok().body(userDTO.get());
    }

    /**
     * DELETE /user:id delete the user by id
     *
     * @param id the id user
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<AtomicInteger> deleteUser(@PathVariable Long id) {
        AtomicInteger count = userService.delete(id);
        if (count.get() <= 0)
            return ResponseEntity.badRequest().body(count);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id + ""))
                .body(count);
    }
}

package com.topica.manage.web;

import com.topica.manage.bean.Classroom;
import com.topica.manage.security.AuthoritiesConstants;
import com.topica.manage.service.ClassroomService;
import com.topica.manage.service.dto.ClassroomDTO;
import com.topica.manage.web.util.HeaderUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("/api/v1")
public class ClassroomResource {
    private static final String ENTITY_NAME = "classroom";
    private static final String DESC = "DESC";
    private final ClassroomService classroomService;

    public ClassroomResource(ClassroomService classroomService) {
        this.classroomService = classroomService;
    }

    /**
     * GET /classroom : get all classroom
     *
     * @return the ResponseEntity with status 200 (OK) and with body all classroom
     */
    @GetMapping("/classroom")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity listUser(@RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
                                   @RequestParam(name = "size", required = false, defaultValue = "10") Integer size,
                                   @RequestParam(name = "sort", required = false, defaultValue = "ASC") String sort) {
        Sort sortable = Sort.by("id").ascending();
        if (sort.equals(DESC)) {
            sortable = Sort.by("id").descending();
        }
        Pageable pageable = PageRequest.of(page, size, sortable);
        final Page<Classroom> listClassroom = classroomService.findAll(pageable);
        return new ResponseEntity<>(listClassroom, HttpStatus.OK);
    }

    /**
     * POST  /classroom: Create a new classroom.
     *
     * @param classroomDTO the classroomDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new classroomDTO, or with status 400 (Bad Request) if the classroom has already or not created.
     */
    @PostMapping("/classroom")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity createClassroom(@RequestBody ClassroomDTO classroomDTO) {
        try {
            ClassroomDTO result = classroomService.save(classroomDTO);
            return ResponseEntity.created(new URI("/api/v1/classroom/" + result.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId() + ""))
                    .body(result);
        } catch (Exception e) {
            return new ResponseEntity<>(Collections.singletonMap("createClassroomFailed", e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * PUT  /classroom: Updates an existing classroom.
     *
     * @param classroomDTO the classroomDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated classroomDTO,
     * or with status 400 (Bad Request) if the classroomDTO is not valid and not update,
     */
    @PutMapping("/classroom")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity updateClassroom(@RequestBody ClassroomDTO classroomDTO) {
        try {
            if (classroomDTO.getId() <= 0) {
                return new ResponseEntity<>(Collections.singletonMap("updateClassroomFailed", "Classroom does not exist!"), HttpStatus.BAD_REQUEST);
            }
            Optional<ClassroomDTO> result = classroomService.update(classroomDTO);
            return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.get().getId() + ""))
                    .body(Optional.of(result).get());
        } catch (Exception e) {
            return new ResponseEntity<>(Collections.singletonMap("updateClassroomFailed",
                    e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * GET  /classroom/:id : get the "id" classroom.
     *
     * @param id the id of the classroomDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the classroomDTO, or with status 404 (Not Found)
     */
    @GetMapping("/classroom/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ClassroomDTO> getClassroom(@PathVariable Long id) {
        Optional<ClassroomDTO> classroomDTO = classroomService.findOne(id);
        return ResponseEntity.ok().body(classroomDTO.get());
    }

    /**
     * GET  /classroom/stu/:id : get the classroom by student id
     *
     * @param id the id of the classroomDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the classroomDTO, or with status 404 (Not Found)
     */
    @GetMapping("/classroom/stu/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Page<Classroom>> getClassroomByStudent(@PathVariable Long id,
                                                                 @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
                                                                 @RequestParam(name = "size", required = false, defaultValue = "10") Integer size,
                                                                 @RequestParam(name = "sort", required = false, defaultValue = "ASC") String sort) {
        Sort sortable = Sort.by("id").ascending();
        if (sort.equals(DESC)) {
            sortable = Sort.by("id").descending();
        }
        Pageable pageable = PageRequest.of(page, size, sortable);
        return ResponseEntity.ok().body(classroomService.findClassroomByStudentId(pageable, id));
    }

    /**
     * DELETE /classroom:id delete the classroom by id
     *
     * @param id the id classroom
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/classroom/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<AtomicInteger> deleteClassroom(@PathVariable Long id) {
        AtomicInteger count = classroomService.delete(id);
        if (count.get() <= 0)
            return ResponseEntity.badRequest().body(count);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id + ""))
                .body(count);
    }
}

package com.topica.manage.web.errors;

public class ClassroomAlreadyExistException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public ClassroomAlreadyExistException() {
        super(ErrorConstants.COURSE_ALREADY_EXIST_TYPE, "Classroom already exists!", "classroomManagement", "classroom-exists");
    }
}

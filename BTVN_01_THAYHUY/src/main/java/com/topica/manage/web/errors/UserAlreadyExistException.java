package com.topica.manage.web.errors;

public class UserAlreadyExistException extends BadRequestAlertException {
    private static final long serialVersionUID = 1L;

    public UserAlreadyExistException() {
        super(ErrorConstants.USER_ALREADY_EXIST_TYPE, "User already exists!", "userManagement", "user-exists");
    }
}

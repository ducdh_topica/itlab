package com.topica.manage.web;

import com.topica.manage.bean.Course;
import com.topica.manage.security.AuthoritiesConstants;
import com.topica.manage.service.CourseService;
import com.topica.manage.service.dto.CourseDTO;
import com.topica.manage.web.util.HeaderUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("/api/v1")
public class CourseResource {
    private static final String ENTITY_NAME = "course";
    private static final String DESC = "DESC";
    private final CourseService courseService;

    public CourseResource(CourseService courseService) {
        this.courseService = courseService;
    }

    /**
     * GET /course : get all course
     *
     * @return the ResponseEntity with status 200 (OK) and with body all course
     */
    @GetMapping("/course")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity listCourse(@RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
                                     @RequestParam(name = "size", required = false, defaultValue = "10") Integer size,
                                     @RequestParam(name = "sort", required = false, defaultValue = "ASC") String sort) {
        Sort sortable = Sort.by("id").ascending();
        if (sort.equals(DESC)) {
            sortable = Sort.by("id").descending();
        }
        Pageable pageable = PageRequest.of(page, size, sortable);
        final Page<Course> listCourse = courseService.findAll(pageable);
        return new ResponseEntity<>(listCourse, HttpStatus.OK);
    }

    /**
     * POST  /course: Create a new course.
     *
     * @param courseDTO the courseDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new courseDTO, or with status 400 (Bad Request) if the course has already or not created.
     */
    @PostMapping("/course")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity createCourse(@RequestBody CourseDTO courseDTO) {
        try {
            CourseDTO result = courseService.save(courseDTO);
            return ResponseEntity.created(new URI("/api/v1/course/" + result.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId() + ""))
                    .body(result);
        } catch (Exception e) {
            return new ResponseEntity<>(Collections.singletonMap("createCourseFailed", e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * PUT  /course : Updates an existing course.
     *
     * @param courseDTO the CourseDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated courseDTO,
     * or with status 400 (Bad Request) if the courseDTO is not valid and not update,
     */
    @PutMapping("/course")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity updateCourse(@RequestBody CourseDTO courseDTO) {
        try {
            if (courseDTO.getId() <= 0) {
                return new ResponseEntity<>(Collections.singletonMap("updateCourseFailed", "Course does not exist!"), HttpStatus.BAD_REQUEST);
            }
            Optional<CourseDTO> result = courseService.update(courseDTO);
            return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, courseDTO.getId() + ""))
                    .body(Optional.of(result).get());
        } catch (Exception e) {
            return new ResponseEntity<>(Collections.singletonMap("updateCourseFailed",
                    e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * GET  /course/:id : get the "id" course.
     *
     * @param id the id of the courseDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the courseDTO, or with status 404 (Not Found)
     */
    @GetMapping("/course/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CourseDTO> getCourse(@PathVariable Long id) {
        Optional<CourseDTO> courseDTO = courseService.findOne(id);
        return ResponseEntity.ok().body(courseDTO.get());
    }

    /**
     * DELETE /course:id delete the course by id
     *
     * @param id the id course
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/course/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<AtomicInteger> deleteCourse(@PathVariable Long id) {
        AtomicInteger count = courseService.delete(id);
        if (count.get() <= 0)
            return ResponseEntity.badRequest().body(count);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id + ""))
                .body(count);
    }
}

package com.topica.manage.service.mapper;

import com.topica.manage.bean.Classroom;
import com.topica.manage.service.dto.ClassroomDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CourseMapper.class, UserMapper.class})
public interface ClassroomMapper extends EntityMapper<ClassroomDTO, Classroom> {
    ClassroomMapper INSTANCE = Mappers.getMapper(ClassroomMapper.class);

    @Mapping(source = "course.id", target = "courseId")
    @Mapping(source = "student.id", target = "studentId")
    @Mapping(source = "teacher.id", target = "teacherId")
    ClassroomDTO toDto(Classroom classroom);

    List<ClassroomDTO> toDto(List<Classroom> classrooms);

    @Mapping(source = "courseId", target = "course")
    @Mapping(source = "studentId", target = "student")
    @Mapping(source = "teacherId", target = "teacher")
    Classroom toEntity(ClassroomDTO classroomDTO);

    List<Classroom> toEntity(List<ClassroomDTO> classroomDTOS);

    default Classroom fromId(Long id) {
        if (id == null) {
            return null;
        }
        Classroom classroom = new Classroom();
        classroom.setId(id);
        return classroom;
    }
}

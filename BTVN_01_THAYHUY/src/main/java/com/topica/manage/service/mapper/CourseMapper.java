package com.topica.manage.service.mapper;

import com.topica.manage.bean.Course;
import com.topica.manage.service.dto.CourseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {})
public interface CourseMapper extends EntityMapper<CourseDTO, Course> {
    CourseMapper INSTANCE = Mappers.getMapper(CourseMapper.class);

    default Course fromId(Long id) {
        if (id == null) {
            return null;
        }
        Course course = new Course();
        course.setId(id);
        return course;
    }
}

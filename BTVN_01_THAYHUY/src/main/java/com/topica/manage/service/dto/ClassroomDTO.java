package com.topica.manage.service.dto;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

public class ClassroomDTO {
    private long id;

    @NotNull
    private Timestamp startTime;

    @NotNull
    private boolean status;

    private Long courseId;
    private Long studentId;
    private Long teacherId;

    public ClassroomDTO() {
    }

    public ClassroomDTO(long id, Timestamp startTime, boolean status, Long courseId, Long studentId, Long teacherId) {
        this.id = id;
        this.startTime = startTime;
        this.status = status;
        this.courseId = courseId;
        this.studentId = studentId;
        this.teacherId = teacherId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public String toString() {
        return "ClassroomDTO{" +
                "id=" + id +
                ", startTime=" + startTime +
                ", status=" + status +
                ", courseId=" + courseId +
                ", studentId=" + studentId +
                ", teacherId=" + teacherId +
                '}';
    }
}

package com.topica.manage.service;

import com.topica.manage.bean.User;
import com.topica.manage.service.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public interface UserService {
    /**
     * Save a user.
     *
     * @param userDTO the entity to save
     * @return the persisted entity
     */
    UserDTO save(UserDTO userDTO);

    /**
     * Get all the user and pagination.
     *
     * @return the list of entities
     */
    Page<User> findAll(Pageable pageable);

    /**
     * Update the years.
     *
     * @param userDTO the pagination information
     * @return the entity after updated
     */
    Optional<UserDTO> update(UserDTO userDTO);


    /**
     * Get the "id" user.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<UserDTO> findOne(Long id);


    /**
     * Delete the "ids" user.
     *
     * @param id the id user
     * @return the count is total all id deleted
     */
    AtomicInteger delete(Long id);
}

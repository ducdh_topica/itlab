package com.topica.manage.service;

import com.topica.manage.bean.Course;
import com.topica.manage.service.dto.CourseDTO;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public interface CourseService {
    /**
     * Save a course.
     *
     * @param courseDTO the entity to save
     * @return the persisted entity
     */
    CourseDTO save(CourseDTO courseDTO);

    /**
     * Get all the course and pagination.
     *
     * @return the list of entities
     */
    Page<Course> findAll(Pageable pageable);

    /**
     * Update the course.
     *
     * @param courseDTO the pagination information
     * @return the entity after updated
     */
    Optional<CourseDTO> update(CourseDTO courseDTO);


    /**
     * Get the "id" course.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<CourseDTO> findOne(Long id);


    /**
     * Delete the "ids" course.
     *
     * @param id the id course
     * @return the count is total all id deleted
     */
    AtomicInteger delete(Long id);
}

package com.topica.manage.service.impl;

import com.topica.manage.bean.Course;
import com.topica.manage.repository.CourseRepository;
import com.topica.manage.service.CourseService;
import com.topica.manage.service.dto.CourseDTO;
import com.topica.manage.service.mapper.CourseMapper;
import com.topica.manage.web.errors.CourseAlreadyExistException;
import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Transactional
public class CourseServiceImpl implements CourseService {
    private final Logger logger = Logger.getLogger(CourseServiceImpl.class);
    private final CourseRepository courseRepository;
    private final CourseMapper courseMapper;

    public CourseServiceImpl(CourseRepository courseRepository, CourseMapper courseMapper) {
        this.courseRepository = courseRepository;
        this.courseMapper = courseMapper;
    }


    /**
     * Save a course.
     *
     * @param courseDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CourseDTO save(CourseDTO courseDTO) {
        logger.debug("Request to save Course : " + courseDTO);
        Optional<Course> courseExist = courseRepository.findById(courseDTO.getId());
        if (courseExist.isPresent()) {
            throw new CourseAlreadyExistException();
        }
        Course course = courseMapper.toEntity(courseDTO);
        return courseMapper.toDto(courseRepository.save(course));
    }

    /**
     * Get all the course.
     *
     * @param pageable the paging and sorting course data
     * @return the page list of entities
     */
    @Override
    public Page<Course> findAll(Pageable pageable) {
        logger.debug("Request to get all Course");
        return courseRepository.findAll(pageable);
    }

    /**
     * Update all information for course and return information after updated
     *
     * @param courseDTO course to update
     * @return updated course
     */
    @Override
    public Optional<CourseDTO> update(CourseDTO courseDTO) {
        return Optional.of(courseRepository
                .findById(courseDTO.getId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(user -> {
                    Course courseUpdated = courseRepository.save(courseMapper.toEntity(courseDTO));
                    logger.debug("Changed Information for Course: " + courseDTO);
                    return courseMapper.toDto(courseUpdated);
                });
    }

    /**
     * Get one course by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<CourseDTO> findOne(Long id) {
        logger.debug("Request to get Course : " + id);
        return courseRepository.findById(id)
                .map(courseMapper::toDto);
    }

    /**
     * Delete the course by id.
     *
     * @param id the id course
     * @return the count id deleted
     */
    @Override
    public AtomicInteger delete(Long id) {
        AtomicInteger count = new AtomicInteger();
        courseRepository.findById(id).ifPresent(course -> {
            courseRepository.delete(course);
            count.getAndIncrement();
            logger.debug("Deleted course: " + id);
        });
        return count;
    }
}

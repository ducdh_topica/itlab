package com.topica.manage.service.impl;

import com.topica.manage.bean.User;
import com.topica.manage.repository.UserRepository;
import com.topica.manage.service.UserService;
import com.topica.manage.service.dto.UserDTO;
import com.topica.manage.service.mapper.UserMapper;
import com.topica.manage.web.errors.UserAlreadyExistException;
import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    private final Logger logger = Logger.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    /**
     * Save a user.
     *
     * @param userDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public UserDTO save(UserDTO userDTO) {
        logger.debug("Request to save User : " + userDTO);
        Optional<User> userExist = userRepository.findById(userDTO.getId());
        if (userExist.isPresent()) {
            throw new UserAlreadyExistException();
        }
        User user = userMapper.toEntity(userDTO);
        return userMapper.toDto(userRepository.save(user));
    }

    /**
     * Get all the user.
     *
     * @param pageable the paging and sorting user data
     * @return the page list of entities
     */
    @Override
    public Page<User> findAll(Pageable pageable) {
        logger.debug("Request to get all User");
        return userRepository.findAll(pageable);
    }

    /**
     * Update all information for user and return information after updated
     *
     * @param userDTO user to update
     * @return updated user
     */
    @Override
    public Optional<UserDTO> update(UserDTO userDTO) {
        return Optional.of(userRepository
                .findById(userDTO.getId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(user -> {
                    User userUpdated = userRepository.save(userMapper.toEntity(userDTO));
                    logger.debug("Changed Information for User: " + userDTO);
                    return userMapper.toDto(userUpdated);
                });
    }

    /**
     * Get one user by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<UserDTO> findOne(Long id) {
        logger.debug("Request to get User : " + id);
        return userRepository.findById(id)
                .map(userMapper::toDto);
    }

    /**
     * Delete the user by id.
     *
     * @param id the id user
     * @return the count id deleted
     */
    @Override
    public AtomicInteger delete(Long id) {
        AtomicInteger count = new AtomicInteger();
        userRepository.findById(id).ifPresent(user -> {
            userRepository.delete(user);
            count.getAndIncrement();
            logger.debug("Deleted user: " + id);
        });
        return count;
    }
}

package com.topica.manage.service.impl;

import com.topica.manage.bean.Classroom;
import com.topica.manage.repository.ClassroomRepository;
import com.topica.manage.service.ClassroomService;
import com.topica.manage.service.dto.ClassroomDTO;
import com.topica.manage.service.mapper.ClassroomMapper;
import com.topica.manage.web.errors.ClassroomAlreadyExistException;
import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Transactional
public class ClassroomServiceImpl implements ClassroomService {
    private static Logger logger = Logger.getLogger(ClassroomServiceImpl.class);
    private final ClassroomRepository classroomRepository;
    private final ClassroomMapper classroomMapper;


    public ClassroomServiceImpl(ClassroomRepository classroomRepository, ClassroomMapper classroomMapper) {
        this.classroomRepository = classroomRepository;
        this.classroomMapper = classroomMapper;
    }

    /**
     * Save a classroom.
     *
     * @param classroomDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ClassroomDTO save(ClassroomDTO classroomDTO) {
        logger.debug("Request to save Classroom : " + classroomDTO);
        Optional<Classroom> classroomExist = classroomRepository.findById(classroomDTO.getId());
        if (classroomExist.isPresent()) {
            throw new ClassroomAlreadyExistException();
        }
        Long timeSystem = Calendar.getInstance().getTimeInMillis();
        Long time = classroomExist.get().getStartTime().getTime();
        if (timeSystem >= time) {
            classroomDTO.setStatus(false);
        }
        Classroom classroom = classroomMapper.toEntity(classroomDTO);
        return classroomMapper.toDto(classroomRepository.save(classroom));
    }

    /**
     * Get all the classroom.
     *
     * @param pageable the paging and sorting classroom data
     * @return the page list of entities
     */
    @Override
    public Page<Classroom> findAll(Pageable pageable) {
        logger.debug("Request to get all Classroom");
        return classroomRepository.findAll(pageable);
    }

    /**
     * Update all information for classroom and return information after updated
     *
     * @param classroomDTO classroom to update
     * @return updated classroom
     */
    @Override
    public Optional<ClassroomDTO> update(ClassroomDTO classroomDTO) {
        return Optional.of(classroomRepository
                .findById(classroomDTO.getId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(classroom -> {
                    Long timeSystem = Calendar.getInstance().getTimeInMillis();
                    Long time = classroom.getStartTime().getTime();
                    if (timeSystem >= time) {
                        classroomDTO.setStatus(false);
                    }
                    Classroom classroomUpdated = classroomRepository.save(classroomMapper.toEntity(classroomDTO));
                    logger.debug("Changed Information for Classroom: " + classroomDTO);
                    return classroomMapper.toDto(classroomUpdated);
                });
    }

    /**
     * Get one classroom by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<ClassroomDTO> findOne(Long id) {
        logger.debug("Request to get Classroom : " + id);
        return classroomRepository.findById(id)
                .map(classroomMapper::toDto);
    }

    /**
     * Delete the classroom by id.
     *
     * @param id the id classroom
     * @return the count id deleted
     */
    @Override
    public AtomicInteger delete(Long id) {
        AtomicInteger count = new AtomicInteger();
        classroomRepository.findById(id).ifPresent(user -> {
            classroomRepository.delete(user);
            count.getAndIncrement();
            logger.debug("Deleted classroom: " + id);
        });
        return count;
    }

    /**
     * Find classroom by student "id"
     *
     * @param pageable paging and sorting classroom data
     * @param id id student
     * @return page list classroom for a kid
     */
    public Page<Classroom> findClassroomByStudentId(Pageable pageable, Long id) {
        logger.debug("Request to get all Classroom");
        return classroomRepository.findClassroomByStudentId(pageable, id);
    }

    /**
     *
     * @param pageable
     * @param timestamp
     * @param status
     * @return
     */
    @Override
    public Page<Classroom> findClassroomByStartTimeOrStatus(Pageable pageable, Timestamp timestamp, boolean status) {
        return null;
    }
}

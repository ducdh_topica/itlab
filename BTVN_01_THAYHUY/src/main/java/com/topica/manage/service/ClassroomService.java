package com.topica.manage.service;

import com.topica.manage.bean.Classroom;
import com.topica.manage.service.dto.ClassroomDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public interface ClassroomService {
    /**
     * Save a classroom.
     *
     * @param classroomDTO the entity to save
     * @return the persisted entity
     */
    ClassroomDTO save(ClassroomDTO classroomDTO);

    /**
     * Get all the classroom and pagination.
     *
     * @return the list of entities
     */
    Page<Classroom> findAll(Pageable pageable);

    /**
     * Update the classroom.
     *
     * @param classroomDTO the pagination information
     * @return the entity after updated
     */
    Optional<ClassroomDTO> update(ClassroomDTO classroomDTO);


    /**
     * Get the "id" classroom.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ClassroomDTO> findOne(Long id);


    /**
     * Delete the "id" classroom.
     *
     * @param id the id of the entity
     * @return the count is total all id deleted
     */
    AtomicInteger delete(Long id);

    /**
     * Find classroom by student "id"
     *
     * @param pageable paging and sorting classroom data
     * @param id       id student
     * @return page list classroom for a kid
     */
    Page<Classroom> findClassroomByStudentId(Pageable pageable, Long id);

    /**
     * @param pageable
     * @param timestamp
     * @param status
     * @return
     */
    Page<Classroom> findClassroomByStartTimeOrStatus(Pageable pageable, Timestamp timestamp, boolean status);
}

package com.topica.manage.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/** Authenticate a user from the database. */
@Component("userDetailsService")
public class DomainUserDetailsService implements UserDetailsService {

  @Value("${spring.account.username}")
  private String username;

  @Value("${spring.account.password}")
  private String password;

  @Value("${spring.account.authority}")
  private String authority;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(final String username) {
    if (!username.equals(this.username)) {
      throw new UsernameNotFoundException("User " + username + " was not found!");
    }
    return createSpringSecurityUser();
  }

  private org.springframework.security.core.userdetails.User createSpringSecurityUser() {
    List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
    SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(this.authority);
    grantedAuthorities.add(simpleGrantedAuthority);

    return new org.springframework.security.core.userdetails.User(
        this.username, this.password, grantedAuthorities);
  }
}

package com.topica.utils;

public class Constant {
    public static final String HOST_NAME = "127.0.0.1";
    public static final Integer PORT_NUMBER = 3000;
    public static final Integer CAPACITY = 10;
    public static final Integer TASKS = 100;
    public static final Integer CORE_POOL_SIZE = 3;
    public static final Integer MAXIMUM_POOL_SIZE = 5;
    public static final Long TIME_KEEP_CONNECT = 500L;
    public static final Long TIME_CONNECT = 2000L;
    public static final String STATUS_WAITING = "WAITING";
    public static final Long TIME_CREATE_TASK = 1000L;
    public static final Long TIME_EXE_TASK = 6000L;
}

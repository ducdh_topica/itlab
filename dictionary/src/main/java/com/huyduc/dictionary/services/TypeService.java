package com.huyduc.dictionary.services;

import com.huyduc.dictionary.beans.Type;
import com.huyduc.dictionary.services.dto.TypeDTO;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public interface TypeService {

  TypeDTO save(TypeDTO typeDTO);

  List<Type> findAll();

  Optional<TypeDTO> update(TypeDTO typeDTO);

  Optional<TypeDTO> findOne(Long id);

  AtomicInteger delete(Long id);
}

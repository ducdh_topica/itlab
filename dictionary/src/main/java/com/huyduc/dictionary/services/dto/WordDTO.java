package com.huyduc.dictionary.services.dto;

import com.huyduc.dictionary.beans.Word;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class WordDTO {
  private long id;

  @NotNull
  @NotBlank
  @Size(max = 50)
  private String en;

  @NotNull
  @NotBlank
  @Size(max = 1000)
  private String vi;

  @NotNull private boolean status;

  @NotNull private String typeName;

  public WordDTO() {}

  public WordDTO(Word word) {
    this.id = word.getId();
    this.en = word.getEn();
    this.vi = word.getVi();
    this.status = word.isStatus();
    this.typeName = word.getType().getName();
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getEn() {
    return en;
  }

  public void setEn(String en) {
    this.en = en;
  }

  public String getVi() {
    return vi;
  }

  public void setVi(String vi) {
    this.vi = vi;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  public String getTypeName() {
    return typeName;
  }

  public void setTypeName(String typeName) {
    this.typeName = typeName;
  }

  @Override
  public String toString() {
    return "WordDTO{"
        + "id="
        + id
        + ", en='"
        + en
        + '\''
        + ", vi='"
        + vi
        + '\''
        + ", status="
        + status
        + ", typeName='"
        + typeName
        + '\''
        + '}';
  }
}

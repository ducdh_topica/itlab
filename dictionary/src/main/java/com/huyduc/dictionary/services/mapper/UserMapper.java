package com.huyduc.dictionary.services.mapper;

import com.huyduc.dictionary.beans.Authority;
import com.huyduc.dictionary.beans.User;
import com.huyduc.dictionary.services.dto.UserDTO;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserMapper {
  public List<UserDTO> usersToUserDTOs(List<User> users) {
    return users.stream()
        .filter(Objects::nonNull)
        .map(this::userToUserDTO)
        .collect(Collectors.toList());
  }

  public UserDTO userToUserDTO(User user) {
    return new UserDTO(user);
  }

  public List<User> userDTOsToUsers(List<UserDTO> userDTOs) {
    return userDTOs.stream()
        .filter(Objects::nonNull)
        .map(this::userDTOToUser)
        .collect(Collectors.toList());
  }

  public User userDTOToUser(UserDTO userDTO) {
    if (userDTO == null) {
      return null;
    } else {
      User user = new User();
      user.setId(userDTO.getId());
      user.setUsername(userDTO.getUsername());
      user.setFirstName(userDTO.getFirstName());
      user.setLastName(userDTO.getLastName());
      user.setBirthOfDate(userDTO.getBirthOfDate());
      user.setGender(userDTO.getGender());
      user.setEmail(userDTO.getEmail());
      user.setAddress(userDTO.getAddress());
      user.setActivated(userDTO.isActivated());
      user.setRegistrationDate(userDTO.getRegistrationDate());
      Set<Authority> authorities = this.authoritiesFromStrings(userDTO.getAuthorities());
      user.setAuthorities(authorities);
      return user;
    }
  }

  private Set<Authority> authoritiesFromStrings(Set<String> authoritiesAsString) {
    Set<Authority> authorities = new HashSet<>();

    if (authoritiesAsString != null) {
      authorities =
          authoritiesAsString.stream()
              .map(
                  string -> {
                    Authority auth = new Authority();
                    auth.setName(string);
                    return auth;
                  })
              .collect(Collectors.toSet());
    }

    return authorities;
  }

  public User userFromId(Long id) {
    if (id == null) {
      return null;
    }
    User user = new User();
    user.setId(id);
    return user;
  }
}

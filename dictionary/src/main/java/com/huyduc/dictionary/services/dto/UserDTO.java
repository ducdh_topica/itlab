package com.huyduc.dictionary.services.dto;

import com.huyduc.dictionary.beans.Authority;
import com.huyduc.dictionary.beans.User;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;
import java.util.stream.Collectors;

public class UserDTO {
  private long id;

  @NotNull
  @NotBlank
  @Size(min = 5, max = 50)
  private String username;

  @NotNull
  @NotBlank
  @Size(min = 5, max = 100)
  private String password;

  @Size(max = 50)
  private String firstName;

  @Size(max = 50)
  private String lastName;

  private Date birthOfDate;

  @NotNull private Boolean gender;

  @Size(max = 100)
  private String email;

  @Size(max = 250)
  private String address;

  @NotNull private boolean activated;

  private Timestamp registrationDate;

  private Set<String> authorities;

  public UserDTO() {}

  public UserDTO(User user) {
    this.id = user.getId();
    this.username = user.getUsername();
    this.password = user.getPassword();
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
    this.birthOfDate = user.getBirthOfDate();
    this.gender = user.getGender();
    this.email = user.getEmail();
    this.address = user.getAddress();
    this.activated = user.isActivated();
    this.registrationDate = user.getRegistrationDate();
    this.authorities =
        user.getAuthorities().stream().map(Authority::getName).collect(Collectors.toSet());
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Date getBirthOfDate() {
    return birthOfDate;
  }

  public void setBirthOfDate(Date birthOfDate) {
    this.birthOfDate = birthOfDate;
  }

  public Boolean getGender() {
    return gender;
  }

  public void setGender(Boolean gender) {
    this.gender = gender;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public boolean isActivated() {
    return activated;
  }

  public void setActivated(boolean activated) {
    this.activated = activated;
  }

  public Timestamp getRegistrationDate() {
    return registrationDate;
  }

  public void setRegistrationDate(Timestamp registrationDate) {
    this.registrationDate = registrationDate;
  }

  public Set<String> getAuthorities() {
    return authorities;
  }

  public void setAuthorities(Set<String> authorities) {
    this.authorities = authorities;
  }

  @Override
  public String toString() {
    return "UserDTO{"
        + "id="
        + id
        + ", username='"
        + username
        + '\''
        + ", password='"
        + password
        + '\''
        + ", firstName='"
        + firstName
        + '\''
        + ", lastName='"
        + lastName
        + '\''
        + ", birthOfDate="
        + birthOfDate
        + ", gender="
        + gender
        + ", email='"
        + email
        + '\''
        + ", address='"
        + address
        + '\''
        + ", activated="
        + activated
        + ", registrationDate="
        + registrationDate
        + ", authorities="
        + authorities
        + '}';
  }
}

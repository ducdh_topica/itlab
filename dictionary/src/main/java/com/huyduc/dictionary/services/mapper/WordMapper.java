package com.huyduc.dictionary.services.mapper;

import com.huyduc.dictionary.beans.Type;
import com.huyduc.dictionary.beans.Word;
import com.huyduc.dictionary.services.dto.WordDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class WordMapper {
  public List<WordDTO> toDto(List<Word> words) {
    return words.stream().filter(Objects::nonNull).map(this::toDto).collect(Collectors.toList());
  }

  public WordDTO toDto(Word word) {
    return new WordDTO(word);
  }

  public List<Word> toEntity(List<WordDTO> wordDTOS) {
    return wordDTOS.stream()
        .filter(Objects::nonNull)
        .map(this::toEntity)
        .collect(Collectors.toList());
  }

  public Word toEntity(WordDTO wordDTO) {
    if (wordDTO == null) {
      return null;
    } else {
      Word word = new Word();
      word.setId(wordDTO.getId());
      word.setEn(wordDTO.getEn());
      word.setVi(wordDTO.getVi());
      word.setStatus(wordDTO.isStatus());
      Type type = new Type();
      type.setName(wordDTO.getTypeName());
      word.setType(type);
      return word;
    }
  }

  public Word wordFromId(Long id) {
    if (id == null) {
      return null;
    }
    Word word = new Word();
    word.setId(id);
    return word;
  }
}

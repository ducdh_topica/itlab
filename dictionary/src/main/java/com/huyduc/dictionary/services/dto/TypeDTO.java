package com.huyduc.dictionary.services.dto;

import com.huyduc.dictionary.beans.Type;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TypeDTO {

  @NotNull
  @NotBlank
  @Size(max = 50)
  private String name;

  public TypeDTO() {}

  public TypeDTO(Type type) {
    this.name = type.getName();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "TypeDTO{" + "name='" + name + '\'' + '}';
  }
}

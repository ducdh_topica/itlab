package com.huyduc.dictionary.services.impl;

import com.huyduc.dictionary.beans.Word;
import com.huyduc.dictionary.repository.WordRepository;
import com.huyduc.dictionary.services.WordService;
import com.huyduc.dictionary.services.dto.WordDTO;
import com.huyduc.dictionary.services.mapper.WordMapper;
import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Transactional
public class WordServiceImpl implements WordService {
  private final WordRepository wordRepository;
  private final WordMapper wordMapper;
  private Logger log = Logger.getLogger(WordServiceImpl.class);

  public WordServiceImpl(WordRepository wordRepository, WordMapper wordMapper) {
    this.wordRepository = wordRepository;
    this.wordMapper = wordMapper;
  }

  @Override
  public WordDTO save(WordDTO wordDTO) {
    log.debug("Request to save Course : " + wordDTO);
    Word word = wordMapper.toEntity(wordDTO);
    return wordMapper.toDto(wordRepository.save(word));
  }

  @Override
  public Page<Word> findAll(Pageable pageable) {
    log.debug("Request to get all words");
    return wordRepository.findAll(pageable);
  }

  @Override
  public Page<Word> searchByEn(Pageable pageable, String search, boolean status) {
    log.debug("Search by en : " + search);
    return wordRepository.findAllByEnStartsWithAndStatus(pageable, search, status);
  }

  @Override
  public Page<Word> searchByVi(Pageable pageable, String search, boolean status) {
    log.debug("Search by vi : " + search);
    return wordRepository.findAllByViStartsWithAndStatus(pageable, search, status);
  }

  @Override
  public Optional<WordDTO> update(WordDTO wordDTO) {
    return Optional.of(wordRepository.findById(wordDTO.getId()))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .map(
            word -> {
              Word wordUpdated = wordRepository.save(wordMapper.toEntity(wordDTO));
              log.debug("Changed Information for Word: " + wordDTO);
              return wordMapper.toDto(wordUpdated);
            });
  }

  @Override
  public Optional<WordDTO> findOne(Long id) {
    log.debug("Request to get word : " + id);
    return wordRepository.findById(id).map(wordMapper::toDto);
  }

  @Override
  public AtomicInteger delete(Long id) {
    AtomicInteger count = new AtomicInteger();
    wordRepository
        .findById(id)
        .ifPresent(
            word -> {
              wordRepository.delete(word);
              count.getAndIncrement();
              log.debug("Deleted word: {}" + id);
            });
    return count;
  }
}

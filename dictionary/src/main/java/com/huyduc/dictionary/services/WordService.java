package com.huyduc.dictionary.services;

import com.huyduc.dictionary.beans.Word;
import com.huyduc.dictionary.services.dto.WordDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public interface WordService {

  WordDTO save(WordDTO wordDTO);

  Page<Word> findAll(Pageable pageable);

  Page<Word> searchByEn(Pageable pageable, String search, boolean status);

  Page<Word> searchByVi(Pageable pageable, String search, boolean status);

  Optional<WordDTO> update(WordDTO wordDTO);

  Optional<WordDTO> findOne(Long id);

  AtomicInteger delete(Long id);
}

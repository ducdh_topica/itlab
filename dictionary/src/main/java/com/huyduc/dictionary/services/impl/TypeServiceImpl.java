package com.huyduc.dictionary.services.impl;

import com.huyduc.dictionary.beans.Type;
import com.huyduc.dictionary.repository.TypeRepository;
import com.huyduc.dictionary.services.TypeService;
import com.huyduc.dictionary.services.dto.TypeDTO;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Transactional
public class TypeServiceImpl implements TypeService {
  private Logger log = Logger.getLogger(TypeServiceImpl.class);
  private final TypeRepository typeRepository;

  public TypeServiceImpl(TypeRepository typeRepository) {
    this.typeRepository = typeRepository;
  }

  @Override
  public TypeDTO save(TypeDTO typeDTO) {
    return null;
  }

  @Override
  public List<Type> findAll() {
    log.debug("Request to get all words");
    return typeRepository.findAll();
  }

  @Override
  public Optional<TypeDTO> update(TypeDTO typeDTO) {
    return Optional.empty();
  }

  @Override
  public Optional<TypeDTO> findOne(Long id) {
    return Optional.empty();
  }

  @Override
  public AtomicInteger delete(Long id) {
    return null;
  }
}

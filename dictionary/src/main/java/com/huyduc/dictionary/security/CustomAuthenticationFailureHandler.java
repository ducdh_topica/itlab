package com.huyduc.dictionary.security;

import com.huyduc.dictionary.security.utils.RequestUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
  private Logger log = Logger.getLogger(CustomAuthenticationFailureHandler.class);
  private final LoginAttemptService loginAttemptService;

  public CustomAuthenticationFailureHandler(LoginAttemptService loginAttemptService) {
    this.loginAttemptService = loginAttemptService;
  }

  public void onAuthenticationFailure(
      HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
      throws IOException, ServletException {

    if (exception.getClass().isAssignableFrom(BadCredentialsException.class)) {
      loginAttemptService.loginFailed(RequestUtils.getClientIP(request));
    }

    if (exception.getMessage() != null && exception.getMessage().equals("block_ip")) {
      response.sendRedirect(request.getContextPath() + "/login?message=block_ip");
      return;
    }
    log.debug("Login failed");
    response.sendRedirect(request.getContextPath() + "/login?message=failed");
  }
}

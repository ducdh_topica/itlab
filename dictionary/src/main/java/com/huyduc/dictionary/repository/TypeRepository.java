package com.huyduc.dictionary.repository;

import com.huyduc.dictionary.beans.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeRepository extends JpaRepository<Type, String> {}

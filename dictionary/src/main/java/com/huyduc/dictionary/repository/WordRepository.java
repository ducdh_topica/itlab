package com.huyduc.dictionary.repository;

import com.huyduc.dictionary.beans.Word;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordRepository extends JpaRepository<Word, Long> {
  Page<Word> findAllByEnStartsWithAndStatus(Pageable pageable, String search, boolean status);

  Page<Word> findAllByViStartsWithAndStatus(Pageable pageable, String search, boolean status);
}

package com.huyduc.dictionary.beans;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "user")
public class User implements Serializable {
  private long id;
  private String username;
  private String password;
  private String firstName;
  private String lastName;
  private Date birthOfDate;
  private Boolean gender;
  private String email;
  private String address;
  private boolean activated;
  private Timestamp registrationDate;
  private Set<Authority> authorities;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @NotNull
  @NotBlank
  @Size(min = 5, max = 50)
  @Column(name = "username", length = 50, unique = true, nullable = false)
  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  @Basic
  @NotNull
  @NotBlank
  @Size(min = 5, max = 100)
  @Column(name = "password", length = 100, nullable = false)
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Basic
  @Size(max = 50)
  @Column(name = "first_name", length = 50)
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  @Basic
  @Size(max = 50)
  @Column(name = "last_name", length = 50)
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @Basic
  @Column(name = "birth_of_date")
  public Date getBirthOfDate() {
    return birthOfDate;
  }

  public void setBirthOfDate(Date birthOfDate) {
    this.birthOfDate = birthOfDate;
  }

  @Basic
  @NotNull
  @Column(name = "gender")
  public Boolean getGender() {
    return gender;
  }

  public void setGender(Boolean gender) {
    this.gender = gender;
  }

  @Basic
  @Size(max = 100)
  @Column(name = "email", length = 100)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Basic
  @Size(max = 250)
  @Column(name = "address", length = 250)
  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  @Basic
  @NotNull
  @Column(name = "activated", nullable = false)
  public boolean isActivated() {
    return activated;
  }

  public void setActivated(boolean activated) {
    this.activated = activated;
  }

  @Basic
  @Column(name = "registration_date")
  public Timestamp getRegistrationDate() {
    return registrationDate;
  }

  public void setRegistrationDate(Timestamp registrationDate) {
    this.registrationDate = registrationDate;
  }

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(
      name = "user_authorities",
      joinColumns = @JoinColumn(name = "user", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "authority", referencedColumnName = "name"))
  public Set<Authority> getAuthorities() {
    return authorities;
  }

  public void setAuthorities(Set<Authority> authorities) {
    this.authorities = authorities;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return id == user.id
        && activated == user.activated
        && Objects.equals(username, user.username)
        && Objects.equals(password, user.password)
        && Objects.equals(firstName, user.firstName)
        && Objects.equals(lastName, user.lastName)
        && Objects.equals(birthOfDate, user.birthOfDate)
        && Objects.equals(gender, user.gender)
        && Objects.equals(email, user.email)
        && Objects.equals(address, user.address)
        && Objects.equals(registrationDate, user.registrationDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        id,
        username,
        password,
        firstName,
        lastName,
        birthOfDate,
        gender,
        email,
        address,
        activated,
        registrationDate);
  }

  @Override
  public String toString() {
    return "User{"
        + "id="
        + id
        + ", username='"
        + username
        + '\''
        + ", password='"
        + password
        + '\''
        + ", firstName='"
        + firstName
        + '\''
        + ", lastName='"
        + lastName
        + '\''
        + ", birthOfDate="
        + birthOfDate
        + ", gender="
        + gender
        + ", email='"
        + email
        + '\''
        + ", address='"
        + address
        + '\''
        + ", activated="
        + activated
        + ", registrationDate="
        + registrationDate
        + ", authorities="
        + authorities
        + '}';
  }
}

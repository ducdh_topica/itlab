package com.huyduc.dictionary.beans;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "word")
public class Word implements Serializable {
  private long id;
  private String en;
  private String vi;
  private boolean status;
  private Type type;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @NotNull
  @NotBlank
  @Size(max = 50)
  @Column(name = "en", nullable = false, length = 50)
  public String getEn() {
    return en;
  }

  public void setEn(String en) {
    this.en = en;
  }

  @Basic
  @NotNull
  @NotBlank
  @Size(max = 1000)
  @Column(name = "vi", nullable = false, length = 1000)
  public String getVi() {
    return vi;
  }

  public void setVi(String vi) {
    this.vi = vi;
  }

  @Basic
  @NotNull
  @Column(name = "status")
  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  @ManyToOne
  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, en, vi, status);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Word word = (Word) o;
    return id == word.id
        && Objects.equals(en, word.en)
        && Objects.equals(vi, word.vi)
        && status == word.status;
  }

  @Override
  public String toString() {
    return "Word{"
        + "id="
        + id
        + ", en='"
        + en
        + '\''
        + ", vi='"
        + vi
        + '\''
        + ", status="
        + status
        + ", type="
        + type
        + '}';
  }
}

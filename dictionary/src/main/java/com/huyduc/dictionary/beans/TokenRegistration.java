package com.huyduc.dictionary.beans;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "token_registration")
public class TokenRegistration implements Serializable {
  private long id;
  private String user;
  private String token;
  private Timestamp dateExpired;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @NotNull
  @Column(name = "user", nullable = false)
  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  @Basic
  @NotNull
  @NotBlank
  @Size(min = 10, max = 100)
  @Column(name = "token", length = 100, nullable = false, unique = true)
  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  @Basic
  @NotNull
  @Column(name = "date_expired", nullable = false)
  public Timestamp getDateExpired() {
    return dateExpired;
  }

  public void setDateExpired(Timestamp dateExpired) {
    this.dateExpired = dateExpired;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TokenRegistration that = (TokenRegistration) o;
    return id == that.id
        && Objects.equals(user, that.user)
        && Objects.equals(token, that.token)
        && Objects.equals(dateExpired, that.dateExpired);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, user, token, dateExpired);
  }

  @Override
  public String toString() {
    return "TokenRegistration{"
        + "id="
        + id
        + ", user='"
        + user
        + '\''
        + ", token='"
        + token
        + '\''
        + ", dateExpired="
        + dateExpired
        + '}';
  }
}

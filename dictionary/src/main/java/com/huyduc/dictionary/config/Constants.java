package com.huyduc.dictionary.config;

public final class Constants {
  static final String DIALECT = "hibernate.dialect";
  static final String SHOW_SQL = "hibernate.show_sql";
  static final String HBM2DDL = "hibernate.hbm2ddl.auto";
  static final String SECOND_LEVEL_CACHE = "hibernate.cache.use_second_level_cache";
  static final String QUERY_CACHE = "hibernate.cache.use_query_cache";

  private Constants() {}
}

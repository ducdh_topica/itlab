package com.huyduc.dictionary.web;

import com.huyduc.dictionary.beans.Type;
import com.huyduc.dictionary.services.TypeService;
import com.huyduc.dictionary.services.WordService;
import com.huyduc.dictionary.services.dto.WordDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Controller
@RequestMapping("/")
public class WordController {
  private final WordService wordService;
  private final TypeService typeService;

  public WordController(WordService wordService, TypeService typeService) {
    this.wordService = wordService;
    this.typeService = typeService;
  }

  @GetMapping(value = "admin/word/create")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public String addNewPage(Model model) {
    List<Type> types = typeService.findAll();

    model.addAttribute("types", types);
    model.addAttribute("typeForm", "add");

    return "form-word";
  }

  @PostMapping(value = "admin/word/create")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public String createWord(
      @ModelAttribute(value = "wordDTO") WordDTO wordDTO, RedirectAttributes redirectAttributes) {
    if (wordDTO == null) {
      redirectAttributes.addFlashAttribute("message", "create failed");

      return "redirect:/admin/word/create";
    }

    wordService.save(wordDTO);

    redirectAttributes.addFlashAttribute("message", "create successful");

    return "redirect:/admin";
  }

  @GetMapping(value = "admin/word/{id}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public String updateWordPage(@PathVariable(name = "id") Long id, Model model) {
    Optional<WordDTO> optionalWordDTO = wordService.findOne(id);

    List<Type> types = typeService.findAll();

    if (optionalWordDTO.isPresent()) {
      model.addAttribute("word", optionalWordDTO.get());
      model.addAttribute("types", types);
      model.addAttribute("typeForm", "update");

      return "form-word";
    }

    return "redirect:/404";
  }

  @PostMapping(value = "admin/word/update")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public String updateWord(
      @ModelAttribute(value = "wordDTO") WordDTO wordDTO, RedirectAttributes redirectAttributes) {

    if (wordDTO == null) {
      redirectAttributes.addFlashAttribute("message", "update failed");
    } else {
      wordService.update(wordDTO);
      redirectAttributes.addFlashAttribute("message", "update successful");
    }
    return "redirect:/admin";
  }

  @GetMapping(value = "admin/word/delete/{id}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public String deleteWord(
      @PathVariable(name = "id") Long id, RedirectAttributes redirectAttributes) {

    AtomicInteger count = wordService.delete(id);

    if (count.get() == 0) {
      redirectAttributes.addFlashAttribute("message", "delete failed");
    } else {
      redirectAttributes.addFlashAttribute("message", "delete successful");
    }

    return "redirect:/admin";
  }
}

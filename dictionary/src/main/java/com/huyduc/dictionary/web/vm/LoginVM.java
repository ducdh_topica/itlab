package com.huyduc.dictionary.web.vm;

import com.huyduc.dictionary.web.common.Constants;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LoginVM {
    @NotNull
    @NotBlank
    @Size(min = Constants.USERNAME_MIN_LEN, max = Constants.USERNAME_MAX_LEN)
    private String username;

    @NotNull
    @NotBlank
    @Size(min = Constants.PASS_MIN_LEN, max = Constants.PASS_MAX_LEN)
    private String password;

    private Boolean rememberMe;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean isRememberMe() {
        return rememberMe;
    }

    public Boolean getRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(Boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    @Override
    public String toString() {
        return "LoginVM{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", rememberMe=" + rememberMe +
                '}';
    }
}

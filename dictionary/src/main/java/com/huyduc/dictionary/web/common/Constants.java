package com.huyduc.dictionary.web.common;

public class Constants {
  public static final int USERNAME_MAX_LEN = 100;
  public static final int USERNAME_MIN_LEN = 5;
  public static final int PASS_MAX_LEN = 100;
  public static final int PASS_MIN_LEN = 5;

  public static final String DESC = "DESC";
  public static final String DEFAULT_SORT = "ASC";
  public static final String DEFAULT_LANGUAGE = "en-vi";
  public static final String VI_EN = "vi-en";
  public static final String DEFAULT_PAGE = "0";
  public static final int DEFAULT_SIZE = 2;
}

package com.huyduc.dictionary.web;

import com.huyduc.dictionary.beans.Word;
import com.huyduc.dictionary.services.WordService;
import com.huyduc.dictionary.web.common.Constants;
import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/")
public class AdminController {
  private final WordService wordService;
  private Logger log = Logger.getLogger(AdminController.class);

  public AdminController(WordService wordService) {
    this.wordService = wordService;
  }

  @GetMapping(value = "admin")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public String homeAdminPage(
      Model model,
      @RequestParam(name = "search", required = false) String search,
      @RequestParam(name = "type", required = false, defaultValue = Constants.DEFAULT_LANGUAGE)
          String type,
      @RequestParam(name = "page", required = false, defaultValue = Constants.DEFAULT_PAGE)
          Integer page,
      @RequestParam(name = "sort", required = false, defaultValue = Constants.DEFAULT_SORT)
          String sort) {

    Sort sortable = Sort.by("id").ascending();

    if (sort.equals(Constants.DESC)) {
      sortable = Sort.by("id").descending();
    }

    Pageable pageable = PageRequest.of(page, Constants.DEFAULT_SIZE, sortable);
    Page<Word> words = null;

    if (type != null) {
      if (type.equals(Constants.DEFAULT_LANGUAGE)) {
        if (search != null) words = wordService.searchByEn(pageable, search, true);
        else words = wordService.findAll(pageable);
      } else if (type.equals(Constants.VI_EN)) {
        words = wordService.searchByVi(pageable, search, true);
      }
    }

    model.addAttribute("type", type);
    model.addAttribute("sort", sort);
    model.addAttribute("page", page);
    model.addAttribute("search", search);
    model.addAttribute("data", words);

    return "home-admin";
  }
}

package com.huyduc.dictionary.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "/")
public class LoginController {

  @GetMapping(value = "login")
  public String login(
      @RequestParam(name = "message", required = false) String message,
      RedirectAttributes redirectAttributes) {
    if (message != null) {
      redirectAttributes.addFlashAttribute("message", "Login failed!");

      return "redirect:/login";
    }

    return "login";
  }

  @GetMapping(value = "403")
  public String homeAdminPage() {

    return "access-denied";
  }
}

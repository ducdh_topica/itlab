package com.huyduc.dictionary.web.exeptions;

public class LoginAlreadyUsedException extends BadRequestAlertException {

  public LoginAlreadyUsedException() {
    super(ErrorConstants.LOGIN_ALREADY_USED_TYPE, "Login already used!", "User", "userexists");
  }
}

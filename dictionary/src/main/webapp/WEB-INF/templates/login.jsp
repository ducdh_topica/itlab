<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>.: Đăng nhập :.</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100i,300,300i,400,400i,500,700,700i,900,900i&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="<c:url value='/resources/bootstrap/css/bootstrap.min.css' />">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <link rel="stylesheet" href="<c:url value='/resources/awesome/font-awesome-4.7.0/css/font-awesome.min.css' />">
    <link rel="stylesheet" href="<c:url value='/resources/css/styles.css' />">
    <link rel="stylesheet" href="<c:url value='/resources/css/responsive.css' />">
</head>
<body id="body-login">
<div class="wrapper-login">
    <img class="bg-image" src="<c:url value='/resources/images/library-869061_1920.jpg' />" alt="anh nen">
    <div class="container">
        <div class="login-form" style="margin-top: 100px;">
            <div class="main-div">
                <div class="panel">
                    <img src="http://careers.topica.asia/assets/img/Logo.png" alt="">
                    <p>Vui lòng điền đầy đủ thông tin đăng nhập.</p>
                    <c:if test="${not empty message}">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Error: </strong> ${message}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </c:if>
                </div>
                <form id="Login" action="<c:url value='loginAction' />" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" id="username" placeholder="Tên đăng nhập"
                               name="username" autofocus required>
                    </div>
                    <div class="form-group form-password">
                        <input type="password" class="form-control" id="password" placeholder="Mật khẩu" name="password"
                               required>
                        <i class="fa fa-eye-slash" aria-hidden="true" id="eye"></i>
                    </div>
                    <div class="forgot">
                        <a href="reset.html">Quên mật khẩu?</a>
                    </div>
                    <sec:csrfInput/>
                    <button type="submit" class="btn btn-primary">ĐĂNG NHẬP <i class="fa fa-arrow-right"
                                                                               aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var visible = false;
        $('#eye').click(function () {
            visible = !visible;
            if (visible) {
                $(this).attr("class", "fa fa-eye");
                $('#password').attr("type", "text");
            } else {
                $(this).attr("class", "fa fa-eye-slash");
                $('#password').attr("type", "password");
            }
        });
    })
</script>
<script type="text/javascript" src="<c:url value='/resources/js/jquery-3.4.1.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/bootstrap/js/bootstrap.min.js' />"></script>
</body>
</html>
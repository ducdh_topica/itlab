<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>.: Admin :.</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100i,300,300i,400,400i,500,700,700i,900,900i&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="<c:url value='/resources/bootstrap/css/bootstrap.min.css' />">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <link rel="stylesheet" href="<c:url value='/resources/awesome/font-awesome-4.7.0/css/font-awesome.min.css' />">
    <link rel="stylesheet" href="<c:url value='/resources/css/admin-styles.css' />">
    <link rel="stylesheet" href="<c:url value='/resources/css/responsive.css' />">
</head>
<body id="body">
<div class="wrapper-h">
    <div class="container-fluid">
        <div class="main-content">
            <div class="container mt-2">
                <!-- Table -->
                <h2 class="mb-5">Dictionary administrator.</h2>
                <div class="header-right text-left">
                    <span>Hello, Admin!</span>
                    <form action="<c:url value='/perform_logout'/>" method="post">
                        <button type="submit" class="btn btn-secondary">Logout</button>
                        <sec:csrfInput/>
                    </form>
                    <br>
                    </a>
                </div>
                <div class="row">
                    <div class="col">
                        <form action="<c:url value="/admin"/>" method="get">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search term..." required>
                                <div class="input-group-btn search-panel">
                                    <select name="type" id="" style="height: 100%; background: white;" required>
                                        <option value="en-vi">English -> Vietnamese</option>
                                        <option value="vi-en">Vietnamese -> English</option>
                                    </select>
                                </div>
                                <span class="input-group-btn"><button class="btn btn-default" style="margin-left: 10px;" type="submit"><span class="glyphicon glyphicon-search">Search</span></button></span>
                            </div>
                        </form>
                    </div>
                </div>
                <br>
                <div class="text-right">
                    <a href="<c:url value='/admin/word/create'/>" class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add new</a>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card shadow" style="margin-top: 20px;">
                            <div class="card-header border-0">
                                <h3 class="mb-0">Dictionary list</h3>
                            </div>
                            <c:if test="${not empty data}">
                                <div class="table-responsive">
                                    <table class="table align-items-center table-flush">
                                        <thead class="thead-light">
                                        <tr>
                                            <c:choose>
                                                <c:when test="${type == 'en-vi'}">
                                                    <th scope="col">English</th>
                                                    <th scope="col">Vietnamese</th>
                                                </c:when>
                                                <c:otherwise>
                                                    <th scope="col">Vietnamese</th>
                                                    <th scope="col">English</th>
                                                </c:otherwise>
                                            </c:choose>
                                            <th scope="col">Word type</th>
                                            <th scope="col">Status</th>
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach var="word" items="${data.getContent()}">
                                            <tr>
                                                <c:choose>
                                                    <c:when test="${type == 'en-vi'}">
                                                        <th scope="row">
                                                            <div class="media align-items-center">
                                                                <div class="media-body">
                                                                    <span class="mb-0 text-sm">${word.getEn()}</span>
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <td>${word.getVi()}</td>
                                                        <td>${word.getType().getName()}</td>
                                                        <td>
                                                          <span class="badge badge-dot mr-4">
                                                            <i class="bg-warning"></i> ${word.isStatus() == true ? 'enabled' : 'disabled'}
                                                          </span>
                                                        </td>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <th scope="row">
                                                                ${word.getVi()}
                                                        </th>
                                                        <td>
                                                            <div class="media align-items-center">
                                                                <div class="media-body">
                                                                    <span class="mb-0 text-sm">${word.getEn()}</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>${word.getType().getName()}</td>
                                                        <td>
                                                          <span class="badge badge-dot mr-4">
                                                            <i class="bg-warning"></i> ${word.isStatus() == true ? 'enabled' : 'disabled'}
                                                          </span>
                                                        </td>
                                                    </c:otherwise>
                                                </c:choose>
                                                <td class="text-right">
                                                    <a class="btn btn-success" href="<c:url value='/admin/word/${word.getId()}'/>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                                    <a class="btn btn-danger" href="<c:url value='/admin/word/delete/${word.getId()}'/>"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-footer py-4">
                                    <nav aria-label="...">
                                        <c:choose>
                                            <c:when test="${not empty search}">
                                                <ul class="pagination justify-content-end mb-0">
                                                    <li class="page-item ${page == 0 ? 'disabled' : ''}">
                                                        <a class="page-link" href="<c:url value='/admin?search=${search}&type=${type}&&page=${page == 0 ? 0 : page - 1}&&sort=${sort}'/>" tabindex="-1">
                                                            <i class="fa fa-angle-left"></i>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                    </li>
                                                    <c:set var="i" value="1"/>
                                                    <c:forEach begin="1" end="${data.getTotalPages()}">
                                                        <li class="page-item ${page == i-1 ? 'active': ''}"><a class="page-link"
                                                                                                               href="<c:url value='/admin?search=${search}&type=${type}&&page=${i-1}&&sort=${sort}'/>">${i}</a>
                                                        </li>
                                                        <c:set var="i" value="${i+1}"/>
                                                    </c:forEach>
                                                    <li class="page-item ${page == data.getTotalPages() - 1 ? 'disabled': ''}">
                                                        <a class="page-link" href="<c:url value='/admin?search=${search}&type=${type}&&page=${page == data.getTotalPages() - 1 ? data.getTotalPages() - 1 : page  + 1}&&sort=${sort}'/>">
                                                            <i class="fa fa-angle-right"></i>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </c:when>
                                            <c:otherwise>
                                                <ul class="pagination justify-content-end mb-0">
                                                    <li class="page-item ${page == 0 ? 'disabled' : ''}">
                                                        <a class="page-link" href="<c:url value='/admin?page=${page == 0 ? 0 : page - 1}&&sort=${sort}'/>" tabindex="-1">
                                                            <i class="fa fa-angle-left"></i>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                    </li>
                                                    <c:set var="i" value="1"/>
                                                    <c:forEach begin="1" end="${data.getTotalPages()}">
                                                        <li class="page-item ${page == i-1 ? 'active': ''}"><a class="page-link"
                                                                                                               href="<c:url value='/admin?page=${i-1}&&sort=${sort}'/>">${i}</a>
                                                        </li>
                                                        <c:set var="i" value="${i+1}"/>
                                                    </c:forEach>
                                                    <li class="page-item ${page == data.getTotalPages() - 1 ? 'disabled': ''}">
                                                        <a class="page-link" href="<c:url value='/admin?page=${page == data.getTotalPages() - 1 ? data.getTotalPages() - 1 : page  + 1}&&sort=${sort}'/>">
                                                            <i class="fa fa-angle-right"></i>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </c:otherwise>
                                        </c:choose>
                                    </nav>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.search-panel .dropdown-menu').find('a').click(function (e) {
            e.preventDefault();
            var param = $(this).attr("href").replace("#", "");
            var concept = $(this).text();
            $('.search-panel span#search_concept').text(concept);
            $('.input-group #search_param').val(param);
        });
    })
</script>
<script type="text/javascript" src="<c:url value='/resources/js/jquery-3.4.1.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/bootstrap/js/bootstrap.min.js' />"></script>
</body>
</html>






<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>.: ${not empty typeForm && typeForm == 'add' ? 'Add new' : 'Update'} :.</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100i,300,300i,400,400i,500,700,700i,900,900i&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="<c:url value='/resources/bootstrap/css/bootstrap.min.css' />">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <link rel="stylesheet" href="<c:url value='/resources/awesome/font-awesome-4.7.0/css/font-awesome.min.css' />">
    <link rel="stylesheet" href="<c:url value='/resources/css/admin-styles.css' />">
    <link rel="stylesheet" href="<c:url value='/resources/css/responsive.css' />">
</head>
<body id="body">
<div class="wrapper-h">
    <div class="container-fluid">
        <div class="main-content">
            <div class="container mt-2">
                <!-- Table -->
                <h2 class="mb-5">Dictionary administrator.</h2>
                <div class="row">
                    <div class="col">
                        <div class="card shadow" style="margin-top: 20px;">
                            <c:choose>
                                <c:when test="${not empty typeForm && typeForm == 'add'}">
                                    <form style="padding: 40px;" action="<c:url value="/admin/word/create"/>" method="post">
                                        <div class="form-group">
                                            <label for="english">English:</label>
                                            <input type="text" maxlength="50" class="form-control" id="english" aria-describedby="en" name="en" placeholder="Enter english" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="vietnamese">Vietnamese:</label>
                                            <input type="text" maxlength="1000" class="form-control" id="vietnamese" name="vi" placeholder="Enter vietnamese" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="type">Word type:</label>
                                            <select class="form-control" id="type" name="typeName" required>
                                                <c:forEach var="type" items="${types}">
                                                    <option value="${type.getName()}">${type.getName()}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="status">Status:</label>
                                            <select class="form-control" id="status" name="status" required>
                                                <option value="true" selected>Enabled</option>
                                                <option value="false">Disabled</option>
                                            </select>
                                        </div>
                                        <sec:csrfInput/>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <button type="reset" class="btn btn-danger">Reset</button>
                                    </form>
                                </c:when>
                                <c:when test="${not empty typeForm && typeForm == 'update'}">
                                    <form style="padding: 40px;" action="<c:url value="/admin/word/update"/>" method="post">
                                        <div class="form-group">
                                            <label for="english1">English:</label>
                                            <input type="text" maxlength="50" class="form-control" id="english1" aria-describedby="en" name="en" placeholder="Enter english" value="${word.getEn()}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="vietnamese1">Vietnamese:</label>
                                            <input type="text" maxlength="1000" class="form-control" id="vietnamese1" name="vi" placeholder="Enter vietnamese" value="${word.getVi()}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="type1">Word type:</label>
                                            <select class="form-control" id="type1" name="typeName" required>
                                                <option value="${word.getTypeName()}">${word.getTypeName()}</option>
                                                <c:forEach var="type" items="${types}">
                                                    <c:if test="${type.getName() != word.getTypeName()}">
                                                        <option value="${type.getName()}">${type.getName()}</option>
                                                    </c:if>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="status1">Status:</label>
                                            <select class="form-control" id="status1" name="status" required>
                                                <c:if test="${word.isStatus() == true}">
                                                    <option value="true" selected>Enabled</option>
                                                    <option value="false">Disabled</option>
                                                </c:if>
                                                <c:if test="${word.isStatus() == false}">
                                                    <option value="false" selected>Disabled</option>
                                                    <option value="true">Enabled</option>
                                                </c:if>
                                            </select>
                                        </div>
                                        <sec:csrfInput/>
                                        <input type="hidden" name="id" value="${word.getId()}"/>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <button type="reset" class="btn btn-danger">Reset</button>
                                    </form>
                                </c:when>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">

</script>
<script type="text/javascript" src="<c:url value='/resources/js/jquery-3.4.1.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/bootstrap/js/bootstrap.min.js' />"></script>
</body>
</html>






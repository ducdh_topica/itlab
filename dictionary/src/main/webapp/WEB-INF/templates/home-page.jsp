<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>.: Laban :.</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100i,300,300i,400,400i,500,700,700i,900,900i&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<c:url value='/resources/bootstrap/css/bootstrap.min.css' />">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <link rel="stylesheet" href="<c:url value='/resources/awesome/font-awesome-4.7.0/css/font-awesome.min.css' />">
    <link rel="stylesheet" href="<c:url value='/resources/css/styles.css' />">
    <link rel="stylesheet" href="<c:url value='/resources/css/responsive.css' />">
</head>
<body id="body">
<div class="wrapper-home">
    <header id="home-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <a href="/dictionary">
                        <img src="https://stc-laban.zdn.vn/dictionary/images/logo.png" alt="logo">
                    </a>
                </div>
                <div class="col-md-9">
                    <div class="header-right text-right">
                        <span>Hello, User!</span>
                        <form action="<c:url value='/perform_logout'/>" method="post">
                            <button type="submit" class="btn btn-secondary">Logout</button>
                            <sec:csrfInput/>
                        </form>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main>
        <div class="main-top">
            <div class="search-top" style="padding: 0 114px;">
                <a href="#" id="ims">
                    <img src="https://dictionary.cambridge.org/vi/external/images/logo-lrg.png?version=4.0.89"
                         alt="Cambridge Dictionary">
                </a>
                <div class="h1 cdo-hero__title">Tạo ý nghĩa cho các từ của bạn</div>
            </div>
            <div class="container">
                <div class="search-input" style="background: #11326f;">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="wrap-input">
                                <form action="<c:url value='/dictionary'/>" method="get" class="text-right"
                                      class="formSearch">
                                    <input type="text" name="search" class="cdo-search__input" id="cdo-search-input"
                                           autocomplete="off" aria-required="true" aria-invalid="false"
                                           placeholder="Tra cứu từ điển..." required>
                                    <span class="cdo-search__controls">
                                        <select name="type" id="selectData" required>
                                            <option value="en-vi">English - Vietnamese</option>
                                            <option value="vi-en">Vietnamese - English</option>
                                        </select>
                                        <button type="submit" class="cdo-search__button"><i class="fa fa-search"
                                                                                            aria-hidden="true"></i></button>
                                    </span>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mod">
                                <div class="pad">
                                    <div class="h2 semi-flush">Từ điển của Tôi</div>
                                    <p>Tạo và chia sẻ danh sách từ ngữ và câu hỏi riêng của bạn miễn phí!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <c:if test="${not empty data}">
                    <h2>Kết quả tìm kiếm cho '${search}': <c:out value="${data.getTotalElements()}"/> kết quả</h2>
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <c:choose>
                                <c:when test="${type == 'en-vi'}">
                                    <th>English</th>
                                    <th><i class="fa fa-arrows-h" aria-hidden="true"></i></th>
                                    <th>Vietnamese</th>
                                    <th>Word type</th>
                                </c:when>
                                <c:otherwise>
                                    <th>Vietnamese</th>
                                    <th><i class="fa fa-arrows-h" aria-hidden="true"></i></th>
                                    <th>English</th>
                                    <th>Word type</th>
                                </c:otherwise>
                            </c:choose>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="word" items="${data.getContent()}">
                            <tr>
                                <c:choose>
                                    <c:when test="${type == 'en-vi'}">
                                        <td><c:out value="${word.getEn()}"/></td>
                                        <td></td>
                                        <td><c:out value="${word.getVi()}"/></td>
                                        <td><c:out value="${word.getType().getName()}"/></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><c:out value="${word.getVi()}"/></td>
                                        <td></td>
                                        <td><c:out value="${word.getEn()}"/></td>
                                        <td><c:out value="${word.getType().getName()}"/></td>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <ul class="pagination">
                        <li class="page-item ${page == 0 ? 'disabled' : ''}"><a class="page-link" href="<c:url value='/dictionary?search=${search}&type=${type}&&page=${page == 0 ? 0 : page - 1}&&sort=${sort}'/>">Previous</a></li>
                        <c:set var="i" value="1"/>
                        <c:forEach begin="1" end="${data.getTotalPages()}">
                            <li class="page-item ${page == i-1 ? 'active': ''}"><a class="page-link"
                                                     href="<c:url value='/dictionary?search=${search}&type=${type}&&page=${i-1}&&sort=${sort}'/>">${i}</a>
                            </li>
                            <c:set var="i" value="${i+1}"/>
                        </c:forEach>
                        <li class="page-item ${page == data.getTotalPages() - 1 ? 'disabled': ''}"><a class="page-link" href="<c:url value='/dictionary?search=${search}&type=${type}&&page=${page == data.getTotalPages() - 1 ? data.getTotalPages() - 1 : page  + 1}&&sort=${sort}'/>">Next</a></li>
                    </ul>
                </c:if>
            </div>
            <div class="bottom section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="copyright">
                                <p>© <span>2019</span> <a href="#" class="transition">DucDH-TOPICA</a> All rights
                                    reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#myTable').DataTable();
    })
</script>
<script type="text/javascript" src="<c:url value='/resources/js/jquery-3.4.1.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/bootstrap/js/bootstrap.min.js' />"></script>
</body>
</html>